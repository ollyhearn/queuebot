from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, BigInteger, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship, backref

import uuid

from db.base import Base


class User(Base):
    __tablename__ = "user"

    id = Column(BigInteger, primary_key=True)
    name = Column(String(64))
    username = Column(String(32))

    owns_queues = relationship("Queue", backref="owner")
    takes_part_in_queues = relationship("QueueUser", backref="user")


class Queue(Base):
    __tablename__ = "queue"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column(String(40), default="Новая очередь")
    description = Column(String(120), default=None)
    owner_id = Column(BigInteger, ForeignKey("user.id"))

    users = relationship(
        "QueueUser", backref="queue"
    )  # TODO: Delete all QueueUser user if Queue deletes


class QueueUser(Base):
    __tablename__ = "queueuser"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    user_id = Column(BigInteger, ForeignKey("user.id"))
    queue_id = Column(UUID(as_uuid=True), ForeignKey("queue.id"))
    position = Column(Integer)
