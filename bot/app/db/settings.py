from dataclasses import dataclass
import os


@dataclass
class Settings:
    dialect: str = os.getenv("DIALECT", "postgresql")
    driver: str = os.getenv("DRIVER", "psycopg2")
    user: str = os.getenv("USER", "user")
    password: str = os.getenv("PASSWORD", "password")
    db_name: str = os.getenv("DB_NAME", "db")
    host: str = os.getenv("HOST", "postgres")
    port: int = os.getenv("PORT", 5432)

    @property
    def uri(self) -> str:
        return f"{self.dialect}+{self.driver}://{self.user}:{self.password}@{self.host}:{self.port}/{self.db_name}"
