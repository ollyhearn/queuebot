from telebot.types import (
    InlineKeyboardButton as button,
    InlineKeyboardMarkup as keyboard,
)
from db.models import Queue


def menu() -> keyboard:
    return keyboard(
        keyboard=[
            [button(text="➕ Новая очередь", callback_data="new_queue")],
            [button(text="📋 Мои очереди", callback_data="my_queues")],
            [button(text="🕓 Текущие очереди", callback_data="parts_queues")],
            [button(text="🔧 Настройки", callback_data="settings")],
            [button(text="ℹ️ О боте", callback_data="about")],
        ]
    )


def my_queues(queues: list[Queue]) -> keyboard:
    kb = [[button(text=q.name, callback_data=f"q:{q.id}")] for q in queues]
    kb.append([button(text="⬅️ В меню", callback_data="to_menu")])
    return keyboard(kb)


def parts_queues(queues: list[Queue]) -> keyboard:
    kb = [[button(text=q.name, callback_data=f"t:{q.id}")] for q in queues]
    kb.append([button(text="⬅️ В меню", callback_data="to_menu")])
    return keyboard(kb)


def queue_part_in_menu() -> keyboard:
    return keyboard(
        keyboard=[
            [button(text="🚪 Выйти из очереди", callback_data="leave_queue")],
            [button(text="🔄 Обновить список", callback_data="refresh_list")],
            [button(text="⬅️ Назад", callback_data="parts_queues")],
        ]
    )


def queue_menu() -> keyboard:
    return keyboard(
        keyboard=[
            [button(text="🔗 Ссылка для вступления", callback_data="get_queue_link")],
            [button(text="▶️ Начать очередь", callback_data="start_queue")],
            [button(text="🫂 Список участников", callback_data="get_queue_users")],
            [button(text="⚙️ Настройки очереди", callback_data="queue_settings")],
            [button(text="⬅️ Назад", callback_data="my_queues")],
        ]
    )


def queue_settings(queue_id: str) -> keyboard:
    return keyboard(
        keyboard=[
            [button(text="✏️ Изменить название", callback_data="edit_queue_name")],
            [
                button(
                    text="📄 Изменить описание", callback_data="edit_queue_description"
                )
            ],
            [button(text="❌ Удалить очередь", callback_data="delete_queue_approve")],
            [button(text="⬅️ Назад", callback_data=f"q:{queue_id}")],
        ]
    )


def approve_queue_delete() -> keyboard:
    return keyboard(
        keyboard=[
            [button(text="✅ Да, удалить очередь", callback_data="delete_queue")],
            [button(text="⬅️ Назад", callback_data="queue_settings")],
        ]
    )


def settings() -> keyboard:
    return keyboard(
        keyboard=[
            [button(text="✏️ Поменять свое имя", callback_data="edit_name")],
            [button(text="⬅️ В меню", callback_data="to_menu")],
        ]
    )


def edit_name() -> keyboard:
    return keyboard(
        keyboard=[
            [button(text="❌ Отмена", callback_data="cancel")],
        ]
    )


def queue_users(queue_id: str) -> keyboard:
    return keyboard(
        keyboard=[
            [button(text="🔃 Поменять позиции", callback_data="swap_users")],
            [button(text="⏩ Кикнуть первого", callback_data="kick_first")],
            [button(text="🔄 Обновить список", callback_data="refresh_users")],
            [button(text="⬅️ Назад", callback_data=f"q:{queue_id}")],
        ]
    )


def your_turn(queue_id: str) -> keyboard:
    return keyboard(
        keyboard=[
            [button(text="Я закончил ⏩", callback_data=f"p:{queue_id}")],
        ]
    )


def to_menu_keyboard() -> keyboard:
    return keyboard(
        keyboard=[
            [button(text="⬅️ В меню", callback_data="to_menu")],
        ]
    )
